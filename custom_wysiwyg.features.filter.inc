<?php

/**
 * Implementation of hook_filter_default_formats().
 */
function custom_wysiwyg_filter_default_formats() {
  $formats = array();

  // Exported format: WYSIWYG
  $formats['wysiwyg'] = array(
    'format' => 'wysiwyg',
    'name' => 'WYSIWYG',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'filter_htmlcorrector' => array(
        'weight' => '10',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
